// TODO: hacer un refactor para extraer en una clase más génerica para todo tipo de collections

import { Cycle } from "./Cycle";

class CycleCollection {

    private items;
    private cursor;

    constructor(cycles) {
        this.items = cycles;
        this.cursor = 0;
    }

    [Symbol.iterator]() {
        let items = this.items;
        let index = this.cursor;
        return {
            next() {
                if (index < items.length) {
                    this.cursor++;
                    let val = items[index];
                    index++;
                    return { value: val, done: false };
                } else return { done: true, value: null };
            }
        };
    }

    public count() {
        return this.items.length;
    }

    public hasPrev() {
        return this.items[this.cursor - 1] !== undefined;
    }

    public hasNext() {
        return this.items[this.cursor + 1] !== undefined;
    }

    // Este método no mueve el cursor hacia atrás como debería hacer un prev. Por ahí habría que pensar en un nombre más apropiado
    public prev() {
        return this.items[this.cursor - 1] || null;
    }
}

export { CycleCollection }