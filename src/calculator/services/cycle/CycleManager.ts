import { Calculator } from "src/calculator/Calculator"
import { CycleGenerator } from "./CycleGenerator";
import { CycleConfig } from "./CycleConfig";
import { InitialCycleNode } from "./InitialCycleNode";
import { Cycle } from "./Cycle";
import { CycleCollection } from "./CycleCollection";

/**
 * CycleManager es un pasamanos que recupera el valor de initialClicles y se lo pasa a CycleGenerator
 * Esta clase itera sobre initialCycles y devuelve los ciclos generados en una collection (CycleCollection)
 * Delega la resposabilidad de crear ciclos en CycleGenerator
 */
class CycleManager {
    private readonly calculator: Calculator;
    private readonly generator: CycleGenerator;

    constructor(calculator: Calculator) {
        this.calculator = calculator;
        this.generator = new CycleGenerator;
    }

    public getCyclesByYear(year): CycleCollection {
        const cycles = this.foreachInitialCycle(cycle => {
            const config = new CycleConfig(cycle, year, this.calculator);
            return this.generator.generate(config);
        });

        return new CycleCollection(cycles);
    }

    // Ejecuta un callback por cada elemento de la variable intialCycles
    // Devuelve un array con la devolución de hacer ejecutado el callback en cada uno de los elementos
    private foreachInitialCycle(callback: CallableFunction) {
        const initialCycles = this.calculator.payload.vacationItem.config.initialCycles;
        
        let cycles = [];

        for (let i = 0; i < initialCycles.length; i++) {
            const node = new InitialCycleNode(
                initialCycles[i - 1] || null,
                initialCycles[i],
                initialCycles[i + 1] || null,
            )
            cycles.push(callback(node));
        }

        return cycles;
    }
}

export { CycleManager }