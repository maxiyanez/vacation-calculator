// Representa un initialCycles que viene de la config de vactionItem que se obtiene de base de datos
// Se usa para poder traquear los nodos previos y siguientes al actual
class InitialCycleNode {
    private readonly prevNode;
    private readonly currentNode;
    private readonly nextNode;

    constructor(prev, current, next) {
        this.prevNode = prev;
        this.currentNode = current;
        this.nextNode = next;
    }

    public prev() {
        return this.prevNode;
    }

    public current() {
        return this.currentNode;
    }

    public next() {
        return this.nextNode;
    }
}

export { InitialCycleNode }
