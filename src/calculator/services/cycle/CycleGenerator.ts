import { CycleConfig } from "./CycleConfig";
import { FutureCycle } from "./strategies/FutureCycle";
import { MonthlyOrDaily } from "./strategies/MonthlyOrDaily";
import { AtSameTime } from "./strategies/AtSameTime";
import { Every20Days } from "./strategies/Every20Days";
import { Cycle } from "./Cycle";
import { Pipeline } from "../../../vendor/pipeline/Pipeline";


// Crea instancias de cycle y setea las prop total, finished, start y end de cada cycle en función diferentes strategies.
// Cada strategy sabe si debe aplicarse o no, el pipeline sólo tiene la resposabilidad de llamarlas en orde de prioridad (si es que lo hay)
// Esta aproximación permite que:
// 1 - El código que evalua que es lo que hay que ser y lo hace, se propaque en varios objetos pequeños, fáciles de leer y mantener
// 2 - Sea posible agregar y/o quitar estas porciones de código (strategies) sin afectar a las otras.
class CycleGenerator {
    public generate(config: CycleConfig): Cycle {
        const pipeline = new Pipeline; // TODO: hay un librería pipeline que viene con node, investigar al respecto (stream)

        return pipeline
            .send(config)
            .through([
                new FutureCycle,
                new MonthlyOrDaily,
                new AtSameTime,
                new Every20Days,
            ])
            .then(function (passable) {
                return passable;
            })
        ;
    }
}

export { CycleGenerator }