import { Cycle } from "../Cycle";
import { CycleConfig } from "../CycleConfig";
import { Strategy } from "./Stragery";

// Las cycle strategies tienen un único método handle. En este método se verifica mediante un if si corresponde a esta strategy aplicarse (y crear el cycle).
// Caso contrario se devuelve la ejecución de la función next para pasar la resposabilidad a la próxima strategy.
class FutureCycle extends Strategy {
    protected mustApply(config: CycleConfig) {
        return config.getCycleStart().isAfter(config.now);
    }

    protected apply(config: CycleConfig) {
        const cycle = {
            start: config.getCycleStart(),
            end: config.getCycleEndDefault(),
        }

        return new Cycle(cycle);
    }
}

export { FutureCycle }