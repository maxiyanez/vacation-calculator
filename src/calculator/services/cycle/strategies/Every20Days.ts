import { GENERATIONDAYS, TYPEOFDAYS } from "src/calculator/config/enums";
import { CycleConfig } from "../CycleConfig";
import { countLaboralDaysBetweenDates } from "src/calculator/utils/date.utils";
import { Cycle } from "../Cycle";
import * as moment from 'moment';
import { Strategy } from "./Stragery";

class Every20Days extends Strategy {
    protected mustApply(config: CycleConfig) {
        return config.getCycle().generationDays === GENERATIONDAYS.DAYS_GENERATES_EVERY_20_DAYS;
    }

    // TODO: el método tiene muchas líneas, ver alternativas para reducirlas
    protected apply(config: CycleConfig): Cycle {
        let days = 0;
        if (config.getCycle().typeOfDays === TYPEOFDAYS.LABORAL_DAYS) {
            days = countLaboralDaysBetweenDates(config.getCycleStart(), config.getCycleEndDefault(), config.calculator.payload.specialDays);
        } else {
            days = moment(config.getCycleEndDefault()).diff(config.getCycleStart(), 'days');
        }

        const periodsOf20DaysPassed = Math.floor(days / 20);

        let totalDays = config.getCycle().baseDays || 0;
        totalDays += periodsOf20DaysPassed * config.getCycle().accumulateDays;

        const cycle = {
            total: totalDays,
            finished: config.isPeriodFinished(config.getCycleEndDefault()),
            start: config.getCycleStart(),
            end: config.getCycleEndDefault(),
        }

        return new Cycle(cycle);
    }
}

export { Every20Days }