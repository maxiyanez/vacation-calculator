import { Cycle } from "../Cycle";
import { CycleConfig } from "../CycleConfig";

// Las cycle strategies tienen un método handle. En este método se verifica mediante un if si corresponde a esta strategy aplicarse (y crear el cycle).
// Caso contrario se devuelve la ejecución de la función next para pasar la resposabilidad a la próxima strategy.
abstract class Strategy {
    public handle(config: CycleConfig, next) {
        let response;

        if (this.mustApply(config)) {
            response = this.apply(config);
        } else {
            response = next(config);
        }

        return response;
    }

    protected abstract mustApply(config: CycleConfig): boolean;

    protected abstract apply(config: CycleConfig): Cycle;
}

export { Strategy }