import { GENERATIONDAYS } from "src/calculator/config/enums";
import { CycleConfig } from "../CycleConfig";
import { Cycle } from "../Cycle";
import { Strategy } from "./Stragery";

class AtSameTime extends Strategy {
    // Determina si se debe aplicar la strategy
    protected mustApply(config: CycleConfig) {
        return config.getCycle().generationDays === GENERATIONDAYS.ALL_DAYS_GENERATES_AT_SAME_TIME;
    }

    // Código propio de la strategy
    protected apply(config: CycleConfig) {
        let totalDays = config.getCycle().baseDays || 0;
        totalDays += config.getCycle().accumulateDays;

        const cycle = {
            total: totalDays,
            finished: config.isPeriodFinished(config.getCycleEndDefault()),
            start: config.getCycleStart(),
            end: config.getCycleEndDefault(),
        }

        return new Cycle(cycle);
    }
}

export { AtSameTime }