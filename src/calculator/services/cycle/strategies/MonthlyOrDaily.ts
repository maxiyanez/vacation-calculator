import { GENERATIONDAYS } from "src/calculator/config/enums";
import { CycleConfig } from "../CycleConfig";
import { Cycle } from "../Cycle";
import * as moment from 'moment';
import { Strategy } from "./Stragery";

class MonthlyOrDaily extends Strategy {
    protected mustApply(config: CycleConfig) {
        return config.getCycle().generationDays === GENERATIONDAYS.DAYS_GENERATES_MONTHLY
            || config.getCycle().generationDays === GENERATIONDAYS.DAYS_GENERATES_DAILY
        ;
    }

    // TODO: el método handle tiene muchas líneas, ver alternativas para reducirlas
    protected apply(config: CycleConfig) {
        let cycleStart = config.getCycleStart();
        let cycleEnd = config.getCycleEndDefault();

        // TODO: me da la sensación que le acceso al payload de la calculadora se puede mejorar
        // Ej.: config.calculator.payload('vactionItem.config.initicalCycle') o config.calculator.getInititalCycles() (puenteando al payload)
        if (config.calculator.payload.vacationItem.config?.initialCycles?.length === 1) {
            if (!config.isFirstYear()) {
                cycleStart = config.getIngressDate().clone().startOf('year');
            }

            cycleEnd = cycleStart.clone().add(1, 'year').startOf('year');

            if (cycleEnd.isAfter(config.now)) {
                cycleEnd = config.now.clone();
            }
        }
        const unit = config.getCycle().generationDays === GENERATIONDAYS.DAYS_GENERATES_MONTHLY
            ? 'months'
            : 'days'
        ;

        // TODO: DEJARLO DINAMICO
        const months = moment(cycleEnd).diff(cycleStart, unit);

        let totalDays = config.getCycle().baseDays || 0;
        totalDays += months * config.getCycle().accumulateDays;

        /*
        Los días se generan todos a la vez: una cumplido el ciclo se activan todos los días de una,
        osea no se van activando proporcionalmente.
        */

        const cycle = {
            total: totalDays,
            finished: config.isPeriodFinished(cycleEnd),
            start: cycleStart,
            end: cycleEnd,
        }

        return new Cycle(cycle);
    }
}

export { MonthlyOrDaily }