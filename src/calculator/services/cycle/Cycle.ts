// Guardan la información ya calculada para un ciclo determinado.
// El dato total va a ser el que finalmente se utilice para calcular el total dentro de PeriodsCalculator
class Cycle {

    public readonly total: number;
    public readonly finished: boolean;
    public readonly start: Date;
    public readonly end: Date;

    constructor({ total = 0, finished = false, start, end }) {
        this.total = total;
        this.finished = finished;
        this.start = start;
        this.end = end;
    }
}

export { Cycle }