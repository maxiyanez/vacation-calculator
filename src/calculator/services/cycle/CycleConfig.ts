import { Calculator } from "src/calculator/Calculator"
import { InitialCycleNode } from "./InitialCycleNode";
import * as moment from 'moment';
import { TIME_INTERVAL_TYPE, TYPE_TO_FORMAT } from "src/calculator/config/enums";

// TODO: pensar otro nombre para la clase

/**
 * Es un contenedor de data con operaciones varias
 * Su principal función es servir como parámetro de los cyles strategies
 * Además engloba operaciones requiridas en varios lugares diferentes
 */

class CycleConfig {
    private node;
    public readonly year: number;
    public readonly calculator: Calculator;
    public readonly now;

    constructor(node: InitialCycleNode, year: number, calculator: Calculator) {
        this.node = node;
        this.year = year;
        this.calculator = calculator;
        this.now = moment();
    }

    public isFirstYear() {
        return this.now.year() == this.year;
    }

    public isPeriodFinished(end) {
        return this.now.isBefore(end);
    }

    public getCycleStart() {
        return this.node.prev()?.end ?? moment({year: this.year}); // TODO: ver si es necesario devolver un formato (ej.: YYYY-MM-DD)
    }

    // Esta es la forma por defecto de calcular en cycleEnd. Según el tipo de ciclo que se este calculando puede ser necesario
    // usar otro procedimiento. Ver: MonthlyOrDaily
    public getCycleEndDefault() {
        const expirationAmount = this.isFirstYear() ? this.getCycle().timeIntervalValue ?? 1 : 1;

        const unit = this.isFirstYear()
            ? TYPE_TO_FORMAT[this.getCycle().timeIntervalType ?? TIME_INTERVAL_TYPE.DAYS]
            : 'years'
        ;

        let cycleEnd = this.getCycleStart().clone().add(expirationAmount, unit)

        if (this.isPeriodFinished(cycleEnd)) {
            cycleEnd = this.now.clone();
        }

        return cycleEnd;
    }

    public getIngressDate() {
        return moment(this.calculator.payload.ingressDate);
    }

    // Acciones con los nodos
    public getPrevCycle() {
        return this.node.prev();
    }

    public getCycle() {
        return this.node.current();
    }

    public getNextycle() {
        return this.node.next();
    }
}

export { CycleConfig }