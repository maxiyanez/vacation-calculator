import { Calculator } from "src/calculator/Calculator"
import { CycleManager } from "./services/cycle/CycleManager";
import * as moment from 'moment';

class PeriodsCalculator {
    private readonly cycleManager: CycleManager;

    constructor(calculator: Calculator) {
        this.cycleManager = new CycleManager(calculator);
    }

    public getTotal(year) {
        let total = 0;

        let cycles = this.cycleManager.getCyclesByYear(year);

        for (const cycle of cycles) {
            if (year == moment().year() && cycles.hasPrev() && !cycles.prev().finished) {
                return total;
            }

            // Si el periodo termina y hay un segundo periodo, el primero se invalida
            if (!cycles.hasNext() || (!cycle.finished && year == moment().year())) {
                total += cycle.total;
            }
        }

        return total;
    }
}

export { PeriodsCalculator }