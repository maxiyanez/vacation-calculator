import * as moment from 'moment';

export function dateIntervalCollide(
  aStart: Date,
  aEnd: Date,
  bStart: Date,
  bEnd: Date,
): boolean {
  if (!aEnd) {
    if (moment(aStart).isBetween(bStart, bEnd, null, '()')) {
      return true;
    }
  }

  if (!bEnd) {
    if (moment(bStart).isBetween(aStart, aEnd, null, '()')) {
      return true;
    }
  }

  if (moment(aStart).isBefore(bStart) && moment(aEnd).isAfter(bEnd)) {
    return true;
  }

  if (moment(bStart).isBefore(aStart) && moment(bEnd).isAfter(aEnd)) {
    return true;
  }

  if (
    moment(bStart).isBefore(aStart) &&
    moment(aStart).isBefore(bEnd) &&
    moment(bEnd).isBefore(aEnd)
  ) {
    return true;
  }

  if (
    moment(aStart).isBefore(bStart) &&
    moment(bStart).isBefore(aEnd) &&
    moment(aEnd).isBefore(bEnd)
  ) {
    return true;
  }

  return false;
}

export const monthFinish = (date: Date, ingressDateMoment: moment.Moment) => {
  return moment(
    `${ingressDateMoment.format('yyyy')}-${moment(date)
      .endOf('month')
      .format('MM-DD')}`,
  );
};

export const monthStart = (date: Date, ingressDateMoment: moment.Moment) => {
  return moment(
    `${ingressDateMoment.add(1, 'year').format('yyyy')}-${moment(date)
      .startOf('month')
      .format('MM-DD')}`,
  );
};

export const countLaboralDaysBetweenDates = (
  from: moment.Moment,
  to: moment.Moment,
  specialDays: Array<Date>,
) => {
  let days = 0;
  const diff = moment(to).diff(from, 'days');
  for (let i = 0; i < diff; i++) {
    const day = from
      .clone()
      .startOf('day')
      .add(i, 'days');
    const weekDay = day.weekday();
    if (
      !specialDays.some(date => day.isSame(date)) &&
      weekDay !== 0 &&
      weekDay !== 6
    ) {
      days++;
    }
  }
  return days;
};

export const calculateDaysBetweenStartAndEnd = (
  days: Array<Date>,
  onlyLaboralDays: boolean,
  specialDays: Array<Date>,
): Array<Date> => {
  const total = [];
  if (!onlyLaboralDays) {
    return days;
  }
  days.forEach(d => {
    const day = moment(d);
    const weekDay = day.weekday();
    while (day.isSameOrBefore(d)) {
      if (
        !specialDays.some(date => day.isSame(date)) &&
        weekDay !== 0 &&
        weekDay !== 6
      ) {
        total.push(d);
      }
      day.add(1, 'days');
    }
  });
  return total;
};

export const dateRange = (
  fromTos: Array<{ start: Date; end: Date }>,
): Date[] => {
  const dates = [];
  fromTos.forEach(fromto => {
    const current = moment(fromto.start);
    let end = moment(fromto.end);
    // if (end.isAfter(moment())) end = moment();
    while (current.isSameOrBefore(end)) {
      dates.push(current.toDate());
      current.add(1, 'day');
    }
  });
  return dates;
};

export const isBetween = (
  date: Date | string | moment.Moment,
  a: Date | string | moment.Moment,
  b: Date | string | moment.Moment,
): boolean => {
  if (!a || !b) return false;
  return moment(date).isBetween(moment(a), moment(b), null, '[]');
};

export const isBefore = (
  a: Date | string | moment.Moment,
  b: Date | string | moment.Moment,
): boolean => {
  if (!a || !b) return false;
  return moment(a).isBefore(moment(b));
};

export const yearsWorked = (ingressDate): number[] => {
  let ingressDateYear = moment(ingressDate).year();
  const currentYear = moment().year();

  let years = [];

  while(ingressDateYear <= currentYear) {
      years.push(ingressDateYear);
      ingressDateYear++;
  }

  return years;
}