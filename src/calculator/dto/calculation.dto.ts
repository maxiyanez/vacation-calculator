export class CalculationDto {

  readonly accumulatedRecords: string;

  readonly records: Object[];

  readonly totalWithoutAdditional: number;

  readonly totalSeniorityDays: number;

  readonly totalGeneratedAdditionalDays: number;

  readonly advancementWasted: number;

  readonly total: number;

  readonly advancementTotal: number;

  readonly totalAvailable: number;

  readonly nextCycleDays: number;

  readonly wasted: number;

  readonly history: Object;
  
}