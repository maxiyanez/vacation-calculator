import { PeriodsCalculator } from './PeriodsCalculator';
import { PayloadDto } from './dto/payload.dto';
import { yearsWorked } from './utils/date.utils';

class Calculator {
    public readonly payload: PayloadDto;
    public readonly periodCalculator: PeriodsCalculator;
    private records;

    constructor(payload: PayloadDto) {
        this.payload = payload;
        this.periodCalculator = new PeriodsCalculator(this);
        this.records = [];
    }

    public calculate() {
        // ¡IMPORTANTE! ya no se usa más el índice del array para identificar a los años (ej.: [0, 1, 2])
        // ahora se usa el año en sí mismo (ej.: [2021, 2022, 2023]) por lo que algunas operaciones para indentificar al primer año cambiaron.
        // Ver ejemplo en el método isFirstYear de CycleConfig
        let years = yearsWorked(this.payload.ingressDate);
        for (let i in years) {
            const regularCycleDays = this.periodCalculator.getTotal(years[i]);

            // TODO: un motón más de cosas

            this.records.push({ regularCycleDays });
        }
    }
}

export { Calculator };