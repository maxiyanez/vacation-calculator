import is_callable from "../helpers/is_callable";

class Pipeline {
    private passable;
    private pipes;
    private method;

    constructor() {
        this.passable = null;
        this.pipes = [];
        this.method = 'handle';
    }
    
    send(passable) {
        this.passable = passable;

        return this;
    }

    through(pipes) {
        this.pipes = Array.isArray(pipes) ? pipes : arguments;

        return this;
    }

    then(destination) {
        let reverse = this.pipes.reverse();

        let pipeline = reverse.reduce(this.carry(), this.prepareDestination(destination));

        return pipeline(this.passable);
    }

    prepareDestination(destination) {
        return function (passable) {
            return destination(passable);
        };
    }

    carry() {
        let method = this.method, params;
        return function (stack, pipe) {
            return function (passable) {
                if (is_callable(pipe)) {
                    return pipe(passable, stack);
                } else {
                    params = [passable, stack];
                }

                return (typeof pipe[method] === 'function')
                    ? pipe[method](...params)
                    : pipe(...params)
                ;
            };
        };
    }
}

export { Pipeline }