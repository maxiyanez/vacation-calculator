const dummydata = {
  "ingressDate": new Date("2021-01-01T12:15:47.454Z"),
  "vacationItem": {
    "_id": "6421e61a9d607c001155b3b1",
    "config": {
      "initialCycles": [
        {
          "accumulateDays": 1.66,
          "typeOfDays": "CALENDAR_DAYS",
          "generationDays": "DAYS_GENERATES_MONTHLY",
          "timeIntervalValue": 0,
          "timeIntervalType": "MONTHS"
        }
      ],
      "seniorityCycles": [
        {
          "accumulatedByCycle": 1,
          "typeOfDays": "CALENDAR_DAYS",
          "daysGeneration": "ALL_DAYS_GENERATES_AT_SAME_TIME",
          "timeInterval": 5,
          "timeIntervalType": "YEARS"
        },
        {
          "accumulatedByCycle": 1,
          "typeOfDays": "CALENDAR_DAYS",
          "daysGeneration": "ALL_DAYS_GENERATES_AT_SAME_TIME",
          "timeInterval": 5,
          "timeIntervalType": "YEARS"
        },
        {
          "accumulatedByCycle": 1,
          "typeOfDays": "CALENDAR_DAYS",
          "daysGeneration": "ALL_DAYS_GENERATES_AT_SAME_TIME",
          "timeInterval": 5,
          "timeIntervalType": "YEARS"
        }
      ],
      "accumulated": {
        "unlimited": false,
        "expires": "NEVER"
      },
      "period": {
        "start": {
          "$date": new Date("2023-01-27T18:49:33.874Z")
        },
        "end": {
          "$date": new Date("2023-12-27T18:49:37.474Z")
        },
        "typeOfCycle": "STARTS_AND_FINISH_AT_DATE"
      },
      "additional": {
        "byBenefit": {
          "value": " "
        },
        "byContract": {
          "value": " "
        }
      },
      "advancement": {
        "maxDaysGenerated": false,
        "allowAdvancement": true,
        "daysAllowedOfNextCycle": 5
      }
    },
    "title": "URUGUAY",
    "externalId": "001",
    "branches": [
      {
        "id": "611fadd525c97e00116c62cd",
        "name": "Montevideo"
      }
    ],
    "countries": ["Uruguay"],
    "positions": ["Customer support"],
    "companyId": "611fa27a5be17a0011ef3c48",
    "createdAt": {
      "$date": "2023-03-27T18:53:14.370Z"
    },
    "updatedAt": {
      "$date": "2023-11-17T16:05:46.869Z"
    },
    "__v": 0
  },
  "specialDays": [
    new Date("2021-12-31T00:00:00.000Z"),
    new Date("2021-12-08T00:00:00.000Z"),
    new Date("2021-12-28T00:00:00.000Z"),
    new Date("2021-12-15T00:00:00.000Z"),
    new Date("2021-12-29T00:00:00.000Z"),
    new Date("2022-12-15T00:00:00.000Z"),
    new Date("2022-12-29T00:00:00.000Z"),
    new Date("2023-02-20T00:00:00.000Z"),
    new Date("2023-01-31T00:00:00.000Z"),
    new Date("2023-02-23T00:00:00.000Z"),
    new Date("2023-05-01T00:00:00.000Z"),
    new Date("2023-08-21T00:00:00.000Z"),
    new Date("2023-08-21T00:00:00.000Z"),
    new Date("2021-12-25T00:00:00.000Z")
  ],
  "considerVacationsSince": new Date("2020-12-31T00:00:00.000Z"),
  "totalRequests": [],
  "history": {
    "availableDays": "5",
    "date": "2023-11-16T17:15:48.054+00:00",
    "createdAt": "2023-11-16T17:15:48.058+00:00",
    "updatedAt": "2023-11-16T17:15:48.058+00:00"
  }
}

export { dummydata }