import { Controller, Get } from '@nestjs/common';
import { Calculator } from './calculator/Calculator';
import { dummydata } from './data/data.1';

@Controller()
export class AppController {
    constructor() { }

    @Get()
    calculate(): string {
        // TODO: dummydata debería venir de consultas a base de datos (cada consulta dentro de su propio service)
        const calculator = new Calculator(dummydata);

        calculator.calculate();

        console.log(calculator);

        return 'Todo bien';
    }
}
